pitcher_urls = [['http://www.fangraphs.com/statsd.aspx?playerid=sa738901&position=P', 'Brown', '[](/prospect-brown)'],
                ['http://www.fangraphs.com/statsd.aspx?playerid=sa3007225&position=P', 'Ashby', '[](/prospect-ashby)'],
                ['http://www.fangraphs.com/statsd.aspx?playerid=sa828707&position=P', 'Supak', '[](/prospect-supak)'],
		['http://www.fangraphs.com/statsd.aspx?playerid=sa876362&position=P', 'Webb', '[](/prospect-webb)']]

position_urls = [['http://www.fangraphs.com/statsd.aspx?playerid=sa738510&position=OF', 'Ray', '[](/prospect-ray)'],
                 ['http://www.fangraphs.com/statsd.aspx?playerid=sa3004968&position=OF', 'Lutz', '[](/prospect-lutz)'],
                 ['http://www.fangraphs.com/statsd.aspx?playerid=sa872722&position=3B', 'Erceg', '[](/prospect-erceg)'],
		 ['http://www.fangraphs.com/statsd.aspx?playerid=sa3008294&position=2B/SS', 'Turang', '[](/prospect-turang)']]

positionl_urls = [[('http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat'
                  '&lg=all&qual=0&type=8&season=2021&month=0&season1=2021'
                  '&ind=0&team=23&rost=0&age=0&filter=&players=0'
                  '&startdate=2021-01-01&enddate=2021-12-31&sort=21,d'), 21, 'WAR'],
                [('http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat'
                  '&lg=all&qual=20&type=8&season=2021&month=0&season1=2021'
                  '&ind=0&team=23&rost=0&age=0&filter=&players=0'
                  '&startdate=2021-01-01&enddate=2021-12-31&sort=21,d'), 17, 'wRC+'],
                [('http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat'
                  '&lg=all&qual=0&type=8&season=2021&month=0&season1=2021'
                  '&ind=0&team=23&rost=0&age=0&filter=&players=0'
                  '&startdate=2021-01-01&enddate=2021-12-31&sort=4,d'), 4, 'HR'],
                [('http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat'
                  '&lg=all&qual=20&type=8&season=2021&month=0&season1=2021'
                  '&ind=0&team=23&rost=0&age=0&filter=&players=0'
                  '&startdate=2021-01-01&enddate=2021-12-31&sort=12,d'), 12, 'AVG']]
pitchingl_urls = [[('https://www.fangraphs.com/leaders.aspx?pos=all&stats=pit'
                    '&lg=all&qual=0&type=8&season=2021&month=0&season1=2021'
                    '&ind=0&team=23&rost=0&age=0&filter=&players=0&sort=20,d'), 20, 'WAR'],
                [('https://www.fangraphs.com/leaders.aspx?pos=all&stats=pit'
                    '&lg=all&qual=10&type=8&season=2021&month=0&season1=2021'
                    '&ind=0&team=23&rost=0&age=0&filter=&players=0&sort=17,a'), 17, 'ERA'],
                [('https://www.fangraphs.com/leaders.aspx?pos=all&stats=pit'
                    '&lg=all&qual=10&type=8&season=2021&month=0&season1=2021'
                    '&ind=0&team=23&rost=0&age=0&filter=&players=0&sort=18,a'), 18, 'FIP'],
                [('https://www.fangraphs.com/leaders.aspx?pos=all&stats=pit'
                    '&lg=all&qual=10&type=8&season=2021&month=0&season1=2021'
                    '&ind=0&team=23&rost=0&age=0&filter=&players=0&sort=8,d'), 8, 'K/9']]
